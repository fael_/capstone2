@extends('layouts.app')
@section('content')
	<div class="container">
	 	<div class="row ">
	 		<div class="col-12 col-md-8 mx-auto my-4 pb-5">
	 			<h3>New Clothing</h3>
	 			<form action="{{route('clothes.store')}}" method="POST" enctype="multipart/form-data">
	 				@csrf
	 				@if($errors->has('name'))
	 					<div class="alert alert-danger">
	 						Clothing Name required.
	 					</div>
	 				@endif
	 				<input type="text" name="name" class="form-control mb-3" id="name" placeholder="Clothing Name" value="{{old('name')}}">
	 				@if($errors->has('price'))
	 					<div class="alert alert-danger">
	 						Clothing Price required.
	 					</div>
	 				@endif
	 				<input type="text" name="price" class="form-control mb-3" id="price" placeholder="Clothing Price" value="{{old('price')}}">
	 				<select name="brand-id" id="brand-id" class="custom-select mb-3">
	 					@foreach($brand as $brand)
	 						<option value="{{$brand->id}}" {{old('brand-id') == $brand->id ? 'selected' : ''}}>{{$brand->name}}</option>
	 					@endforeach
	 				</select>
	 				<select name="stock-id" id="stock-id" class="custom-select mb-3">
	 					@foreach($stock as $stock)
	 						<option value="{{$stock->id}}" {{old('stock-id') == $stock->id ? 'selected' : ''}}>{{$stock->name}}</option>
	 					@endforeach
	 				</select>
	 				@if($errors->has('image'))
	 					<div class="alert alert-danger">
	 						Clothing Image required.
	 					</div>
	 				@endif
	 				<input type="file" name="image" class="form-control-file mb-3" id="image">
	 				@if($errors->has('description'))
	 					<div class="alert alert-danger">
	 						Clothing Description required.
	 					</div>
	 				@endif
	 				<textarea name="description" id="description" cols="10" rows="10" class="form-control mb-3" placeholder="Product Description">{{old('description')}}</textarea>
	 				<button type="submit" class="btn btn-custom">Add New Clothing</button>
	 			</form>
	 		</div>
	 	</div>
	</div>
@endsection