@extends('layouts.app')
@section('content')
@if($clothe)
<div class="container my-4 pb-5">
    <p style="display: none;">{{$clothe}}</p>
    <div class="row">
        {{-- @include('clothes.includes.error-status') --}}
        {{-- start of cards --}}
        <div class="col-12">
            <div class="card">
                <img src="/public/{{$clothe->image}}" alt="" class="card-img-top">
                
                <div class="card-body">
                    <h5 class="card-title">{{$clothe->name}}</h5>
                    <p class="card-text">PHP{{number_format($clothe->price,2)}}</p>
                    <p class="card-text">{{$clothe->brand->name}}</p>
                    <p class="card-text">{{$clothe->description}}</p>
                </div>
                <div class="card-footer">
                    <a href="{{route('clothes.index')}}" class="btn btn-custom w-100 mb-2">Show all Clothes</a>
                </div>
            </div>
        </div>
        {{-- end of cards --}}
    </div>
</div>
@else
    404 Not Found
@endif
@endsection

