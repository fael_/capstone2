@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col col-12 text-center mt-5">
			{{-- input form --}}
			<form action="{{route('brands.update',['brand' => $brand->id])}}" method="POST">
				@csrf
				@method('PATCH')
				<label for="name">Brand Name:</label>
				<input type="text" name="name" value={{$brand->name}} class="form-control">
				<button type="sumbit" class="btn-custom mt-4">Update Brand</button>
			</form>
		</div>
	</div>
</div>

@endsection