@extends('layouts.app')
@section('content')
	<div class="container my-4 pb-5">
		<div class="row">
			<div class="col-12">
				<h3>My Cart</h3>
			</div>
			
			@if(Session::has('transaction'))
			<div class="col-12">
				{{-- table --}}
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover text-center">
						<thead>
							<th scope="col">Name</th>
							<th scope="col">Price per unit</th>
							<th scope="col">Quantity</th>
							<th scope="col">Subtotal</th>
							<th scope="col">action</th>
						</thead>
						<tbody>
							@foreach($clothe as $clothe)
								{{-- start of row --}}
							<tr>
								<th scope="row">{{$clothe->name}}</th>
								<td>&#8369;<span>{{ number_format($clothe->price,2) }}</span></td>
								<td>
									<div class="add-tocart-field mb-1">
										<form action="{{route('transactions.update',['transaction' => $clothe->id])}}" method="POST">
											@method('PATCH')
											@csrf
											<input type="number" name="quantity" id="quantity" class="form-control" value="{{$clothe->quantity}}" min="1" required>
											<button class="btn btn-inverted w-100" type="submit">Edit</button>
										</form>
									</div>
								</td>
								<td>
									&#8369;{{number_format($clothe->subtotal,2)}}
								</td>
								<td>
									<form action="{{route('transactions.destroy',['transaction' => $clothe->id])}}" method="POST">
										@csrf
										@method('DELETE')
										<button class="btn btn-delete2 w-100">Remove from cart</button>
									</form>
								</td>
							</tr>
							{{-- end of row --}}
	
							@endforeach							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" class="text-right">Total</td>
								<td>&#8369;<span id="total">{{ number_format($total,2) }}</span></td>
							</tr>
						</tfoot>
					</table>
				</div>
				{{-- end of table --}}
				<form action="{{route('transactions.empty')}}" method="POST">
					@csrf
					@method('DELETE')
					<button type="submit" class="btn btn-delete">Clear Cart</button>
				</form>
				<hr>
			</div>
			<div class="col-12 mt-4">
				<h3>Request Form</h3>
	 			<form action="{{route('transactions.store')}}" method="POST" enctype="multipart/form-data">
	 				@csrf
	 				<input type="text" name="name" class="form-control mb-3" id="name" placeholder="Your Full Name" value="{{old('name')}}" required>
	 				<input type="text" name="address" class="form-control mb-3" id="address" placeholder="Full Address" value="{{old('address')}}" required>
	 				<input type="number" name="number" class="form-control mb-3" id="number" placeholder="Your Mobile Number" value="{{old('number')}}" required>
	 				<textarea name="reason" id="reason" cols="10" rows="10" class="form-control mb-3" placeholder="Describe your purposes for renting" required>{{old('reason')}}</textarea>
	 				@can('isLogged')
						<button class="btn btn-custom">Request to Rent</button>
					@endcan
					@cannot('isLogged')
						<a href="{{route('login')}}" class="w-100 btn btn-inverted">Login to Continue</a>
					@endcannot
					<small class="w-100 text-center">*After request has been made. Kindly wait 2-3 days for approval.</small>
	 			</form>
			</div>
			@else
			<div class="col-12">
				<div class="alert alert-info">
					Cart Empty
				</div>
			</div>
			@endif
		</div>
	</div>
@endsection