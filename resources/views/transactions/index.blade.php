@extends('layouts.app')
@section('content')
<div class="container my-4 pb-5">
    <div class="row">
        <div class="col-12">
			<h3>Requests</h3>
        </div>
        <div class="col-12 mt-2">
            <div class="accordion" id="accordionExample" data-aos="fade-up">
				@foreach($transactions as $transaction)
					@if(Auth::user()->name == $transaction->user->name || Auth::user()->name == 'Admin')
                {{-- start of accordion --}}
                  <div class="card">
                    <div class="card-header" id="headingOne">
                      <h2 class="mb-0">
                            <button class="btn btn-link text-decoration-none text-dark" type="button" data-toggle="collapse" data-target="#transaction-{{$transaction->id}}" aria-expanded="true" aria-controls="collapseOne">
                              {{$transaction->created_at->format('F d, Y H:i:s')}} <span class="badge badge-light align-self-center">{{$transaction->status->name}}</span>
                            </button>	
                      </h2>
                    </div>
                    <div id="transaction-{{$transaction->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body">
                        {{-- start of table --}}
                        <hr style="border: 3px solid black; border-style: dotted;">
                        <div class="table-responsive">
                            {{-- start of transaction table --}}
                            <table class="table table-striped table-light">
                                    <tbody>
                                        <tr>
                                            <td>Username:</td>
                                            <td>{{$transaction->user->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Transaction Code:
                                            </td>
                                            <td>
                                                {{$transaction->transaction_code}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Customer Name:
                                            </td>
                                            <td>
                                                {{$transaction->name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Address:
                                            </td>
                                            <td>
                                                {{$transaction->address}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Contact Number:
                                            </td>
                                            <td>
                                                {{$transaction->number}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Reasoning:
                                            </td>
                                            <td>
                                                {{$transaction->reason}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Date of Purchase:
                                            </td>
                                            <td>
                                                {{$transaction->created_at->format('F d, Y (D)')}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Status:
                                            </td>
                                            <td>
                                                {{$transaction->status->name}}
                                            </td>
                                        </tr>
                                    </tbody>
                            </table>
                            {{-- end of trans table --}}
                            {{-- {{dd($transacion->products)}} --}}
                            {{-- start of prod trans table --}}
                            <table class="table table-striped table-hover table-light">
                                <thead>
                                    <th scope="row">Clothing Name:</th>
                                    <th scope="row">Price:</th>
                                    <th scope="row">Quantity:</th>
                                    <th scope="row">Subtotal:</th>
                                </thead>
                                <tbody>
                                    {{-- start of prod trans details --}}
                                    @foreach($transaction->clothes as $transaction_clothe)
                                    <tr>
                                        <td>
                                            {{$transaction_clothe->name}}
                                        </td>
                                        <td>
                                            PHP{{number_format($transaction_clothe->pivot->price,2)}}
                                        </td>
                                        <td>
                                            {{$transaction_clothe->pivot->quantity}}
                                        </td>
                                        <td>
                                            PHP{{number_format($transaction_clothe->pivot->subtotal,2)}}
                                        </td>
                                    </tr>
                                    @endforeach
                                    {{-- end of prod trans dtails --}}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3" class="text-right"><strong>Total</strong></td>
                                        <td>PHP{{number_format($transaction->total,2)}}</td>
                                    </tr>
                                </tfoot>
                            </table>
                            <hr style="border: 3px solid black; border-style: dotted;">
                            @can('isAdmin')
                            <form action="{{route('transactions.update',['transaction' => $transaction->id])}}" method="POST">
                                @csrf 
                                @method('PATCH')
                                <input style="display:none;" type="number" name="status" id="status" class="form-control" value="2" min="1">
                                <button type="submit" class="btn btn-custom {{($transaction->status->name == 'Cancelled' || $transaction->status->name == 'Complete' || $transaction->status->name == 'Cancelled' ? 'disabled' : '')}}">Approve</button>
                            </form>
                            <form action="{{route('transactions.update',['transaction' => $transaction->id])}}" method="POST">
                                @csrf 
                                @method('PATCH')
                                <input style="display:none;" type="number" name="status" id="status" class="form-control" value="3" min="1">
                                <button type="submit" class="btn btn-delete2 {{($transaction->status->name == 'Cancelled' || $transaction->status->name == 'Complete' || $transaction->status->name == 'Cancelled' ? 'disabled' : '')}}">Deny</button>
                            </form>
                            @endcan
                            @cannot('isAdmin')
                            <form action="{{route('transactions.update',['transaction' => $transaction->id])}}" method="POST">
                                @csrf 
                                @method('PATCH')
                                <input style="display:none;" type="number" name="status" id="status" class="form-control" value="5" min="1">
                                <button type="submit" class="btn btn-delete {{($transaction->status->name != 'Pending' ? 'disabled' : '')}}">Cancel</button>
                            </form>
                            <form action="{{route('transactions.update',['transaction' => $transaction->id])}}" method="POST">
                                @csrf 
                                @method('PATCH')
                                <input style="display:none;" type="number" name="status" id="status" class="form-control" value="4" min="1">
                                <button type="submit" class="btn btn-inverted {{($transaction->status->name != 'Approved' ? 'disabled' : '')}}">Complete</button>
                            </form>
                            @endcannot
                            {{-- end of prod trans table --}}
                        </div>
                        {{-- end of table --}}
                      </div>
                    </div>
				  </div>
				  @else 
				  @endif
                  {{-- end of accordion --}}
                  @endforeach
				</div>
        </div>
    </div>
</div>
@endsection