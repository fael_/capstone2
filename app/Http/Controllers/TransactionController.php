<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use Str;
use DB;
use App\Clothe;
use App\Transaction;
use App\Status;
use App\Stock;

class TransactionController extends Controller
{
    public function index(Transaction $transaction)
    {
        $statuses = Status::all();
        $transactions = Transaction::all();
        if(Auth::user()->role_id == 1)
        {
            $transactions;
        }
        else if (Auth::user()->role_id === null)
        {
            $transactions->whereIn('user_id', Auth::user()->id);
        }
        $statuses = Status::all();
        return view('transactions.index')->with('transactions',$transactions)->with('statuses',$statuses);
    }

    public function update(Request $request, $id)
    {
        if($request->quantity != null)
        {
            $request->validate([
                'quantity' => 'required|min:1'
            ]);
            // $id = $clothe->id;
            $qty = $request->quantity;
            // store to session
            $request->session()->put("transaction.$id",$qty);
    
            // dd($request->session()->get('cart'));
            return redirect(route('transactions.create'));
        }
        else
        {            
            $request->validate([
                'status' => 'required|min:1'
            ]);

            $transactions = Transaction::all();
            
            DB::table('transactions')
            ->where('id', $id)
            ->update(['status_id' => $request->status]);

            return view('transactions.index')->with('transactions',$transactions);
        }
    }

    public function destroy(Request $request, $id)
    {
    	$request->session()->forget("transaction.$id");

        if(count($request->session()->get('transaction')) == 0)
        {
            $request->session()->forget('transaction');
        }

        return redirect(route('transactions.create'));
    }
    public function create()
    {
        if (Session::has('transaction'))
        {
            // get all id of cart session
            $clothe_ids = array_keys(Session::get('transaction'));
            // query to db
            // get all products that are in the session cart
            $clothes = Clothe::find($clothe_ids);

            $total = 0;
            foreach($clothes as $clothe){
                $clothe->quantity = Session::get("transaction.$clothe->id");
                $clothe->subtotal = $clothe->quantity * $clothe->price;
                $total += $clothe->subtotal;
            }

            // dd($products);
            return view('transactions.create')
            ->with('clothe',$clothes)
            ->with('total',$total);
        }
        else
        {
            return view('transactions.create');
        }
    }

    public function store(Request $request)
    {
        $clothe_ids = array_keys(Session::get('transaction'));
        $clothes = Clothe::find($clothe_ids);
        $total = 0;
        $transaction = new Transaction;
        $transaction->user_id = Auth::user()->id;
        $transaction->transaction_code = Auth::user()->id . Str::random(10);

        $transaction->save();

        foreach ($clothes as $clothe)
        {
            
            $clothe->quantity = Session::get("transaction.$clothe->id");
            $clothe->subtotal = $clothe->price * $clothe->quantity;
            $total += $clothe->subtotal;
            $transaction->clothes()->attach($clothe->id,["quantity" => $clothe->quantity, "subtotal" => $clothe->subtotal, "price" => $clothe->price]);

        }
        $transaction->stock_id = 1;
        $transaction->status_id = 1;
        $transaction->name = $request->input('name');
        $transaction->address = $request->input('address');
        $transaction->number = $request->input('number');
        $transaction->reason = $request->input('reason');
        $transaction->total = $total;

        $transaction->save();
        
        Session::forget("transaction");

        return redirect(route('transactions.index'));
    }

    public function show(Transaction $transaction)
    {
        
    }

    public function empty()
    {
    	Session::forget('transaction');
        return redirect(route('transactions.create'));
    }
}
