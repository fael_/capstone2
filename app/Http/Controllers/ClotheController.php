<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Clothe;
use App\Brand;
use App\Stock;
use Illuminate\Http\Request;

class ClotheController extends Controller
{
    public function index()
    {   
        $clothe = Clothe::all();

        return view('clothes.index')->with('clothes',$clothe)->with('brands',Brand::all())->with('stocks',Stock::all());
    }
    public function create(Clothe $clothe)
    {
        $this->authorize('create',$clothe);
    	$brand = Brand::all();
        $stock = Stock::all();
    	return view('clothes.create')->with('brand',$brand)->with('stock',$stock);
    }
    public function store(Request $request, Clothe $clothe, Stock $stock)
    {
        $this->authorize('create',$clothe,$stock);
        	$request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
            'brand-id' => 'required|numeric',
            'stock-id' => 'required|numeric',
            'description' => 'required|string',
            'image' => 'required|image|max:5000'
        ]);

        $clothe = new Clothe;

        $clothe->name = $request->input('name');
        $clothe->price = $request->input('price');
        $clothe->brand_id = $request->input('brand-id');
        $clothe->stock_id = $request->input('stock-id');
        $clothe->status_id = 1;
        $clothe->description = $request->input('description');
        $clothe->image = $request->image->store('public');

        $clothe->save();

        return redirect(route('clothes.index'));
    }

    public function edit(Clothe $clothe)
    {
        $this->authorize('update',$clothe);
        $brand = Brand::all();
        $stock = Stock::all();
        return view('clothes.edit', compact('clothe'))->with('brand',$brand)->with('stock',$stock);
    }

    public function update(Request $request, Clothe $clothe)
    {   
        if($request->input('name') != null)
        {
            $request->validate([
                'name' => 'required|string',
                'price' => 'required|numeric',
                'brand-id' => 'required|numeric',
                'stock-id' => 'required|numeric',
                'description' => 'required|string',
                'image' => 'image|max:5000'
            ]);
            
            $clothe->name = $request->input('name');
            $clothe->price = $request->input('price');
            $clothe->brand_id = $request->input('brand-id');
            $clothe->stock_id = $request->input('stock-id');
            $clothe->description = $request->input('description');
    
            if ($request->hasFile('image'))
            {
                $clothe->image = $request->image->store('clothes');
            }
    
            $clothe->save();
            
            $request->session()->flash('status','Updated successfully!');
    
            return redirect(route('clothes.edit',['clothe'=>$clothe->id]));
        }     
        else
        {
            $request->validate([
                'stock-id' => 'required|numeric'
            ]);
            
            $clothe->stock_id = $request->input('stock-id');
            $clothe->save();
            // DB::table('clothes')
            // ->where('id', $clothe->id)
            // ->update(['stock_id' => $request->input('stock-id')]);
            
            return redirect(route('clothes.index',['clothe' => $clothe->id]));
        }
    }

    public function show(Clothe $clothe)
    {
        return view('clothes.show')->with('clothe',$clothe);
    }

    public function destroy(Clothe $clothe)
    {
        $this->authorize('delete',$clothe);
        $clothe->delete();
        return redirect (route('clothes.index'));
    }
}
