<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
class BrandController extends Controller
{
	public function index()
	{
		$brand = Brand::all();
		return view('brands.index', compact('brand'));
	}

    public function create(Brand $brand)
    {
		$this->authorize('create',$brand);
    	return view('brands.create');
    }

    public function store(Request $request)
    {
    	$name = $request->input('name');
    	$request->validate([
    		'name' => 'string|required|max:50|unique:brands,name'
    	]);
    	$brand = new Brand;
    	$brand->name = $name;
    	$brand->save();
    	return redirect(route('brands.index'));
    }

    public function edit(Brand $brand)
    {
		$this->authorize('update',$brand);
    	return view('brands/edit')->with('brand',$brand);
    	return redirect(route('brands.index'));
    }

    public function update(Request $request, Brand $brand)
    {
    	$request->validate([
    		'name' => 'string|required|max:50|unique:brands,name'
    	]);
    	$brand->name = $request->input('name');
    	$brand->save();
    	return redirect(route('brands.index',['brand' => $brand->id]));
    }

    public function destroy(Brand $brand)
    {
		$this->authorize('delete',$brand);
    	$brand->delete();
    	return redirect (route('brands.index',['brand' => $brand->id]));
    }
}
